#API REST - Inventario de vacunación de empleados

El proyecto esta desarrollado en Spring Boot con el fin de proveer servicios API REST en relacion a datos requeridos de la vacunación de empleados y la gestión de esta información de manera que un administrador pueda registrar, actualizar y eliminar a usuarios que tengan rol de empleado asi como filtrar informacion respecto a su estado de vacunación.

La estructura del proyecto esta dada a partir de la carpeta src donde se encuentran 2 subcarpetas (java y test), la carpeta java contiene la parte del desarrollo de los servicios y la carpeta test que contiene las pruebas unitarias que se desarrollaron para verificar el correcto funcionameinto de los servicios.

Para desarrollar la estructura del proyecto primeramente se hizo el modelo de los datos y a partir de este se diseñó la base de datos en **Postgresql**, la cual contempla las siguientes tablas **usuario**, **rol**, **detalle_vacuna**, **tipo_vacuna**.


Luego de esto se desarrollaron las entidades en el proyecto a partir de las tablas definidas en la base de datos, tambien se implementaron los pojos DTO y VO para los registros y visualizaciones de los datos.

Posteriormente se realizarón los repositorios a partir de JPA ademas de los servicios y controller para realizar las peticiones de los servicios.

# Modo de ejecución

- Para probar el proyecto primerameinte se debe importar el script de la base de datos en DBMS Postgresql

- Posterior a esto actualizar las variables de entorno del archivo .env con el usuario y contraseña del DBMS

- Finalemente se debe ejecutar la clase **InventarioVacunacionGtApplication.java**

- Esto levantara el servicio en el puerto 8080 por tanto en el navegador se debe ingresar a localhost:8080

- En la direccion http://localhost:8080/swagger-ui/ esta la documentacion Swagger donde se pueden visualizar los servicios API REST.
