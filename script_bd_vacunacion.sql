PGDMP     +    &                {            bd_vacunacion    14.5    14.5 &               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    147912    bd_vacunacion    DATABASE     k   CREATE DATABASE bd_vacunacion WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Spanish_Bolivia.1252';
    DROP DATABASE bd_vacunacion;
                postgres    false                        2615    147913 	   seguridad    SCHEMA        CREATE SCHEMA seguridad;
    DROP SCHEMA seguridad;
                postgres    false                        2615    147915 
   vacunacion    SCHEMA        CREATE SCHEMA vacunacion;
    DROP SCHEMA vacunacion;
                postgres    false            �            1259    147956    rol    TABLE     Z   CREATE TABLE seguridad.rol (
    id integer NOT NULL,
    nombre character varying(50)
);
    DROP TABLE seguridad.rol;
    	   seguridad         heap    postgres    false    7            �            1259    147955 
   rol_id_seq    SEQUENCE     �   CREATE SEQUENCE seguridad.rol_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE seguridad.rol_id_seq;
    	   seguridad          postgres    false    7    214                       0    0 
   rol_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE seguridad.rol_id_seq OWNED BY seguridad.rol.id;
       	   seguridad          postgres    false    213            �            1259    147917    usuario    TABLE     }  CREATE TABLE seguridad.usuario (
    id integer NOT NULL,
    cedula bigint NOT NULL,
    nombres character varying(255) NOT NULL,
    apellidos character varying(255) NOT NULL,
    correo_electronico character varying(50) NOT NULL,
    username character varying(50),
    password character varying(255),
    created_by character varying(50),
    created_date timestamp without time zone,
    updated_by character varying(50),
    updated_date timestamp without time zone,
    id_rol integer NOT NULL,
    fecha_nacimiento date,
    direccion character varying(255),
    telefono character varying(20),
    estado_vacunacion boolean
);
    DROP TABLE seguridad.usuario;
    	   seguridad         heap    postgres    false    7            �            1259    147916    usuario_id_seq    SEQUENCE     �   CREATE SEQUENCE seguridad.usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE seguridad.usuario_id_seq;
    	   seguridad          postgres    false    212    7                       0    0    usuario_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE seguridad.usuario_id_seq OWNED BY seguridad.usuario.id;
       	   seguridad          postgres    false    211            �            1259    148027    detalle_vacuna    TABLE     c  CREATE TABLE vacunacion.detalle_vacuna (
    id integer NOT NULL,
    fecha_vacuna date,
    numero_dosis integer,
    id_tipo_vacuna integer NOT NULL,
    created_by character varying(50),
    created_date timestamp without time zone,
    updated_by character varying(50),
    updated_date timestamp without time zone,
    id_usuario integer NOT NULL
);
 &   DROP TABLE vacunacion.detalle_vacuna;
    
   vacunacion         heap    postgres    false    4            �            1259    148026    detalle_vacuna_id_seq    SEQUENCE     �   CREATE SEQUENCE vacunacion.detalle_vacuna_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE vacunacion.detalle_vacuna_id_seq;
    
   vacunacion          postgres    false    218    4                       0    0    detalle_vacuna_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE vacunacion.detalle_vacuna_id_seq OWNED BY vacunacion.detalle_vacuna.id;
       
   vacunacion          postgres    false    217            �            1259    148011    tipo_vacuna    TABLE       CREATE TABLE vacunacion.tipo_vacuna (
    id integer NOT NULL,
    nombre character varying,
    created_by character varying(50),
    created_date timestamp without time zone,
    updated_by character varying(50),
    updated_date timestamp without time zone
);
 #   DROP TABLE vacunacion.tipo_vacuna;
    
   vacunacion         heap    postgres    false    4            �            1259    148010    tipo_vacuna_id_seq    SEQUENCE     �   CREATE SEQUENCE vacunacion.tipo_vacuna_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE vacunacion.tipo_vacuna_id_seq;
    
   vacunacion          postgres    false    216    4                       0    0    tipo_vacuna_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE vacunacion.tipo_vacuna_id_seq OWNED BY vacunacion.tipo_vacuna.id;
       
   vacunacion          postgres    false    215            n           2604    147959    rol id    DEFAULT     f   ALTER TABLE ONLY seguridad.rol ALTER COLUMN id SET DEFAULT nextval('seguridad.rol_id_seq'::regclass);
 8   ALTER TABLE seguridad.rol ALTER COLUMN id DROP DEFAULT;
    	   seguridad          postgres    false    214    213    214            m           2604    147920 
   usuario id    DEFAULT     n   ALTER TABLE ONLY seguridad.usuario ALTER COLUMN id SET DEFAULT nextval('seguridad.usuario_id_seq'::regclass);
 <   ALTER TABLE seguridad.usuario ALTER COLUMN id DROP DEFAULT;
    	   seguridad          postgres    false    212    211    212            p           2604    148030    detalle_vacuna id    DEFAULT     ~   ALTER TABLE ONLY vacunacion.detalle_vacuna ALTER COLUMN id SET DEFAULT nextval('vacunacion.detalle_vacuna_id_seq'::regclass);
 D   ALTER TABLE vacunacion.detalle_vacuna ALTER COLUMN id DROP DEFAULT;
    
   vacunacion          postgres    false    218    217    218            o           2604    148014    tipo_vacuna id    DEFAULT     x   ALTER TABLE ONLY vacunacion.tipo_vacuna ALTER COLUMN id SET DEFAULT nextval('vacunacion.tipo_vacuna_id_seq'::regclass);
 A   ALTER TABLE vacunacion.tipo_vacuna ALTER COLUMN id DROP DEFAULT;
    
   vacunacion          postgres    false    215    216    216                      0    147956    rol 
   TABLE DATA           ,   COPY seguridad.rol (id, nombre) FROM stdin;
 	   seguridad          postgres    false    214   .       
          0    147917    usuario 
   TABLE DATA           �   COPY seguridad.usuario (id, cedula, nombres, apellidos, correo_electronico, username, password, created_by, created_date, updated_by, updated_date, id_rol, fecha_nacimiento, direccion, telefono, estado_vacunacion) FROM stdin;
 	   seguridad          postgres    false    212   O.                 0    148027    detalle_vacuna 
   TABLE DATA           �   COPY vacunacion.detalle_vacuna (id, fecha_vacuna, numero_dosis, id_tipo_vacuna, created_by, created_date, updated_by, updated_date, id_usuario) FROM stdin;
 
   vacunacion          postgres    false    218   {/                 0    148011    tipo_vacuna 
   TABLE DATA           i   COPY vacunacion.tipo_vacuna (id, nombre, created_by, created_date, updated_by, updated_date) FROM stdin;
 
   vacunacion          postgres    false    216   �/                  0    0 
   rol_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('seguridad.rol_id_seq', 2, true);
       	   seguridad          postgres    false    213                       0    0    usuario_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('seguridad.usuario_id_seq', 10, true);
       	   seguridad          postgres    false    211                       0    0    detalle_vacuna_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('vacunacion.detalle_vacuna_id_seq', 2, true);
       
   vacunacion          postgres    false    217                       0    0    tipo_vacuna_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('vacunacion.tipo_vacuna_id_seq', 4, true);
       
   vacunacion          postgres    false    215            v           2606    147961    rol rol_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY seguridad.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id);
 9   ALTER TABLE ONLY seguridad.rol DROP CONSTRAINT rol_pkey;
    	   seguridad            postgres    false    214            r           2606    156255 "   usuario usuario_cedula_cedula1_key 
   CONSTRAINT     s   ALTER TABLE ONLY seguridad.usuario
    ADD CONSTRAINT usuario_cedula_cedula1_key UNIQUE (cedula) INCLUDE (cedula);
 O   ALTER TABLE ONLY seguridad.usuario DROP CONSTRAINT usuario_cedula_cedula1_key;
    	   seguridad            postgres    false    212            t           2606    147924    usuario usuario_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY seguridad.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 A   ALTER TABLE ONLY seguridad.usuario DROP CONSTRAINT usuario_pkey;
    	   seguridad            postgres    false    212            z           2606    148032 "   detalle_vacuna detalle_vacuna_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY vacunacion.detalle_vacuna
    ADD CONSTRAINT detalle_vacuna_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY vacunacion.detalle_vacuna DROP CONSTRAINT detalle_vacuna_pkey;
    
   vacunacion            postgres    false    218            x           2606    148018    tipo_vacuna tipo_vacuna_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY vacunacion.tipo_vacuna
    ADD CONSTRAINT tipo_vacuna_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY vacunacion.tipo_vacuna DROP CONSTRAINT tipo_vacuna_pkey;
    
   vacunacion            postgres    false    216            {           2606    147962    usuario id_rol    FK CONSTRAINT     z   ALTER TABLE ONLY seguridad.usuario
    ADD CONSTRAINT id_rol FOREIGN KEY (id_rol) REFERENCES seguridad.rol(id) NOT VALID;
 ;   ALTER TABLE ONLY seguridad.usuario DROP CONSTRAINT id_rol;
    	   seguridad          postgres    false    212    214    3190            |           2606    148033 1   detalle_vacuna detalle_vacuna_id_tipo_vacuna_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY vacunacion.detalle_vacuna
    ADD CONSTRAINT detalle_vacuna_id_tipo_vacuna_fkey FOREIGN KEY (id_tipo_vacuna) REFERENCES vacunacion.tipo_vacuna(id) NOT VALID;
 _   ALTER TABLE ONLY vacunacion.detalle_vacuna DROP CONSTRAINT detalle_vacuna_id_tipo_vacuna_fkey;
    
   vacunacion          postgres    false    218    3192    216            }           2606    148060 -   detalle_vacuna detalle_vacuna_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY vacunacion.detalle_vacuna
    ADD CONSTRAINT detalle_vacuna_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES seguridad.usuario(id) NOT VALID;
 [   ALTER TABLE ONLY vacunacion.detalle_vacuna DROP CONSTRAINT detalle_vacuna_id_usuario_fkey;
    
   vacunacion          postgres    false    3188    212    218               )   x�3�t�-�IMLɏ��2�tL����,.)
q��qqq �S
t      
     x�u�Mn�0���)�@� �]P���.�.ٌRR�\S)��6Me�@�b��潙W�</J��#<O�|���Qu�E��^7���P^J�2!eE����5�т]>{=�N��m�"�ٔ�"~N!�w;{a�Ѷ�ޔd�Zr�?R��5w8��jh��;���H��DD�E�f<p���V��.78�/��h~ ��9�d$���U�w�宅�'K�u<�(K&���M
��(��3
u���p���ԑߣ�s�z'"��E�#H�Rf�0wc�%I�\��?         4   x�3�4�41�50�5��4�?2�2�4202�50�529�Q���b���� ��
         B   x�3�.(-�����#.#N�⒢ĨԼ��Dc΀�̪�"AN������<5(�"���� ��     