package com.challenge.invvacunaciongt.inventariovacunaciongt.service.seguridad;

import com.challenge.invvacunaciongt.inventariovacunaciongt.commons.ResultResponse;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.seguridad.Usuario;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.enums.ResponseUsuarioService;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.dto.UsuarioDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.text.SimpleDateFormat;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;



@SpringBootTest
class UsuarioServiceTest {
    @Autowired
    UsuarioService usuarioService;

    //@Test
    //    void findByEstadoVacunacion(){

    //}*

    //@Test
    void registrarEmpleadoConDatosValidos() {
        UsuarioDTO dto = new UsuarioDTO();
        dto.setCedula(1892734564);
        dto.setNombres("Lucas");
        dto.setApellidos("Duran");
        dto.setCorreoElectronico("lucas@gmail.com");
        dto.setIdRol(1);
        ResponseUsuarioService res = usuarioService.persistRegistrarEmpleado(dto);
        assertEquals(ResponseUsuarioService.EMPLEADO_REGISTRADO, res);
    }
    //@Test
    void RegistrarEmpleadoConCedulaDiferenteA10Digitos() {
        UsuarioDTO dto = new UsuarioDTO();
        dto.setCedula(189765446);
        dto.setNombres("Lucas");
        dto.setApellidos("sda");
        dto.setCorreoElectronico("lucas@gmail.com");
        dto.setIdRol(1);
        ResponseUsuarioService res = usuarioService.persistRegistrarEmpleado(dto);
        assertEquals(ResponseUsuarioService.CEDULA_NO_VALIDA, res);
    }
    //@Test
    void RegistrarEmpleadoDeNombreConValoresNumericosOCaracteresEspeciales() {
        UsuarioDTO dto = new UsuarioDTO();
        dto.setCedula(1446769980);
        dto.setNombres("luv6789");
        dto.setApellidos("@##$%jhjhjk");
        dto.setCorreoElectronico("lucas@gmail.com");
        dto.setIdRol(1);
        ResponseUsuarioService res = usuarioService.persistRegistrarEmpleado(dto);
        assertEquals(ResponseUsuarioService.DATOS_PERSONALES_NO_VALIDOS, res);
    }
    //@Test
    void update() {
        String actualizacionCorreo = "mateo@gmail.com";
        Optional<Usuario> entity = usuarioService.findById(3);
        if (entity.isPresent()) {
            entity.get().setCorreoElectronico(actualizacionCorreo);
            usuarioService.update(entity.get());
            Optional<Usuario> entityUpdated = usuarioService.findById(3);
            assertEquals(entityUpdated.get().getCorreoElectronico(), actualizacionCorreo);
        }
    }

    @Test
    void update2() {
        SimpleDateFormat fechaNacimiento = new SimpleDateFormat("08/01/2000");
        Optional<Usuario> entity = usuarioService.findById(3);
        if (entity.isPresent()) {
            entity.get().setFechaNacimiento(fechaNacimiento.get2DigitYearStart());
            entity.get().setDireccion("Av. America");
            entity.get().setTelefono("76627993");
            entity.get().setEstadoVacunacion(true);
            usuarioService.update(entity.get());
            Optional<Usuario> entityUpdated = usuarioService.findById(3);
            assertEquals(entityUpdated.get().isEstadoVacunacion(), true);
        }
    }


}