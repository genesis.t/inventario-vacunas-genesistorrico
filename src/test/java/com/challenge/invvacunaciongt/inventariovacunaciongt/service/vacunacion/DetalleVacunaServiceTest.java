package com.challenge.invvacunaciongt.inventariovacunaciongt.service.vacunacion;

import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.vacunacion.DetalleVacuna;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.enums.ResponseUsuarioService;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.dto.UsuarioDTO;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.vacunacion.dto.DetalleVacunaDTO;
import com.challenge.invvacunaciongt.inventariovacunaciongt.service.seguridad.UsuarioService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class DetalleVacunaServiceTest {

    @Autowired
    DetalleVacunaService detalleVacunaService;

    @Test
    void registrarDetalleVacuna() {
        SimpleDateFormat fechaVacuna = new SimpleDateFormat("03/01/2022");
        DetalleVacunaDTO dto = new DetalleVacunaDTO();
        dto.setFechaVacuna(fechaVacuna.get2DigitYearStart());
        dto.setNumeroDosis(1);
        dto.setIdTipoVacuna(1);
        dto.setIdUsuario(2);
        DetalleVacuna entity = detalleVacunaService.persistRegistroDetalleVacuna(dto);
        assertNotNull(entity);

    }

}