package com.challenge.invvacunaciongt.inventariovacunaciongt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InventarioVacunacionGtApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventarioVacunacionGtApplication.class, args);
	}

}
