package com.challenge.invvacunaciongt.inventariovacunaciongt.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HttpResponseMessage {
    FIND_SUCCESSFUL("Busqueda exitosa"),
    PERSIST_SUCCESSFUL("Se registro correctamente"),
    UPDATE_SUCCESSFUL("Se actualizo correctamente"),
    DELETE_SUCCESSFUL("Se elimino correctamente"),

    NOT_FOUND_RECORD("No se encontro el registro"),

    CEDULA_NO_VALIDA("El valor debe poseer 10 digitos"),
    DATOS_PERSONALES_NO_VALIDOS("Los campos nombres y apellidos no pueden contener numeros o caracteres especiales"),

    PERSON_RECORD_EXISTENT("El documento de la persona ya se encuentra registrado"),
    NOT_FOUND_USUARIO("No se encontro el registro del usuario: ");
    private final String value;
}
