package com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.vacunacion;

import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.SchemaBD;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(schema = SchemaBD.VACUNACION, name ="tipo_vacuna")
public class TipoVacuna {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
    @Column(name="created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name="created_by")
    private String createdBy;
    @Column(name="updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name="updated_by")
    private String updatedBy;

}
