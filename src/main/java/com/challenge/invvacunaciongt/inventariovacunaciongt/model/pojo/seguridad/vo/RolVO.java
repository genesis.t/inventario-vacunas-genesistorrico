package com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RolVO {
    private int id;
    private String nombre;
}
