package com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.vacunacion.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class DetalleVacunaDTO {
    private Date fechaVacuna;
    private int numeroDosis;
    private int idTipoVacuna;
    private int idUsuario;
}
