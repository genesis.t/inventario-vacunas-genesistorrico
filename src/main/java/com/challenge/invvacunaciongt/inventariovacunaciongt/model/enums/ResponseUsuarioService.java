package com.challenge.invvacunaciongt.inventariovacunaciongt.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseUsuarioService {
    EMPLEADO_REGISTRADO,
    CEDULA_NO_VALIDA,
    DATOS_PERSONALES_NO_VALIDOS,
    CORREO_NO_VALIDO

}
