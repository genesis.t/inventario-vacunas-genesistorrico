package com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.vacunacion.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TipoVacunaDTO {
    private String nombre;
}
