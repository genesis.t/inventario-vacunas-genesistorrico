package com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.vacunacion.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TipoVacunaVO {
    private int id;
    private String nombre;
}
