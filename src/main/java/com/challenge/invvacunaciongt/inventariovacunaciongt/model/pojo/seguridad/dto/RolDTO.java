package com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RolDTO {
    private String nombre;
}
