package com.challenge.invvacunaciongt.inventariovacunaciongt.model.repository.seguridad;

import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.seguridad.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface RolRepository extends JpaRepository<Rol, Integer> {
}

