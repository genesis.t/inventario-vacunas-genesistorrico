package com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.vacunacion.vo;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public class DetalleVacunaVO {
    private int id;
    private Date fechaVacuna;
    private int numeroDosis;
    private int idTipoVacuna;
    private int idUsuario;
}
