package com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.vo;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public class UsuarioVO {
    private int id;
    private long cedula;
    private String nombres;
    private String apellidos;
    private String correoElectronico;
    private String username;
    private String password;
    private Date fechaNacimiento;
    private String direccion;
    private String telefono;
    private int idRol;
    private boolean estadoVacunacion;
}
