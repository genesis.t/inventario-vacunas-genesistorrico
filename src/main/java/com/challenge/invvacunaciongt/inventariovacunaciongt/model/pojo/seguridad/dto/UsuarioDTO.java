package com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;
import java.util.Date;
import javax.validation.constraints.*;


@Getter
@Setter
public class UsuarioDTO {

    @NotNull
    @NotEmpty
    @Min(1000000000)
    @Max(1999999999)
    private long cedula;
    @NotNull
    @NotEmpty
    private String nombres;
    @NotNull
    @NotEmpty
    private String apellidos;
    @NotNull
    @NotEmpty
    @Email
    private String correoElectronico;
    private String username;
    private String password;
    private Date fechaNacimiento;
    private String direccion;
    private String telefono;
    private int idRol;
    private boolean estadoVacunacion;
}