package com.challenge.invvacunaciongt.inventariovacunaciongt.model.repository.vacunacion;

import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.vacunacion.DetalleVacuna;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetalleVacunaRepository extends JpaRepository<DetalleVacuna,Integer> {

}
