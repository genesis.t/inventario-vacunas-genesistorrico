package com.challenge.invvacunaciongt.inventariovacunaciongt.model.repository.seguridad;

import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.seguridad.Usuario;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.vo.UsuarioVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
    List<Usuario> findAllByIdRol(int idRol);
    List<Usuario> findAllByEstadoVacunacion(boolean estadoVacunacion);

}
