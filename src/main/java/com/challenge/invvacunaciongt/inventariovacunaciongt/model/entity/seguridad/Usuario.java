package com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.seguridad;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.SchemaBD;
import lombok.*;
import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(schema = SchemaBD.SEGURIDAD, name ="usuario")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private long cedula;
    private String nombres;
    private String apellidos;
    @Column(name="correo_electronico")
    private String correoElectronico;
    @Column(name="fecha_nacimiento")
    private Date fechaNacimiento;
    private String direccion;
    private String telefono;
    private String username;
    private String password;
    @Column(name="estado_vacunacion")
    private boolean estadoVacunacion;
    @Column(name="id_rol")
    private int idRol;
    @Column(name="created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name="created_by")
    private String createdBy;
    @Column(name="updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name="updated_by")
    private String updatedBy;

}
