package com.challenge.invvacunaciongt.inventariovacunaciongt.controller.vacunacion;


import com.challenge.invvacunaciongt.inventariovacunaciongt.commons.ResultResponse;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.vacunacion.DetalleVacuna;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.enums.HttpResponseMessage;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.vacunacion.dto.DetalleVacunaDTO;
import com.challenge.invvacunaciongt.inventariovacunaciongt.service.vacunacion.DetalleVacunaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/detallev")
public class DetalleVacunaController {
    private final DetalleVacunaService detalleVacunaService;

   @Autowired
   public DetalleVacunaController(DetalleVacunaService detalleVacunaService) {
        this.detalleVacunaService = detalleVacunaService;
    }


    @PostMapping
    public ResponseEntity<?> persist(@RequestBody DetalleVacunaDTO dto) {
        DetalleVacuna entity = detalleVacunaService.persistRegistroDetalleVacuna(dto);
        return new ResponseEntity<>(ResultResponse.builder().status(true).message(HttpResponseMessage.PERSIST_SUCCESSFUL.getValue()).data(entity.getId()).build(), HttpStatus.CREATED);
    }

}
