package com.challenge.invvacunaciongt.inventariovacunaciongt.controller.seguridad;

import com.challenge.invvacunaciongt.inventariovacunaciongt.commons.ResultResponse;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.seguridad.Usuario;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.enums.HttpResponseMessage;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.enums.ResponseUsuarioService;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.dto.UsuarioDTO;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.vo.UsuarioVO;
import com.challenge.invvacunaciongt.inventariovacunaciongt.service.seguridad.UsuarioService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/usuario")
public class UsuarioController {
    private final UsuarioService usuarioService;

    @Autowired
    public UsuarioController(UsuarioService empresaService) {
        this.usuarioService = empresaService;
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUsuario(@PathVariable("id") int id, @RequestBody UsuarioDTO dto) {
        Optional<Usuario> entity = usuarioService.findById(id);
        if (entity.isPresent()) {
            BeanUtils.copyProperties(dto, entity.get());
            usuarioService.update(entity.get());
            return new ResponseEntity<>(ResultResponse.builder().status(true).message(HttpResponseMessage.UPDATE_SUCCESSFUL.getValue()).build(), HttpStatus.OK);

        }
        return new ResponseEntity<>(ResultResponse.builder().status(false).message(HttpResponseMessage.NOT_FOUND_RECORD.getValue()).build(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @CrossOrigin
    public ResponseEntity<?> delete(@PathVariable("id") int id) {
        Optional<Usuario> entity = usuarioService.findById(id);
        if (entity.isPresent()) {
            usuarioService.delete(entity.get());
            return new ResponseEntity<>(ResultResponse.builder().status(true).message(HttpResponseMessage.DELETE_SUCCESSFUL.getValue()).build(), HttpStatus.OK);
        }
        return new ResponseEntity<>(ResultResponse.builder().status(false).message(HttpResponseMessage.NOT_FOUND_RECORD.getValue()).build(), HttpStatus.OK);
    }


    @PostMapping
    public ResponseEntity<?> persist(@RequestBody UsuarioDTO dto) {
        ResponseUsuarioService res = usuarioService.persistRegistrarEmpleado(dto);
        if (res.equals(ResponseUsuarioService.CEDULA_NO_VALIDA)) {
            return new ResponseEntity<>(ResultResponse.builder().status(false).message(HttpResponseMessage.CEDULA_NO_VALIDA.getValue()).build(), HttpStatus.BAD_REQUEST);
        }
        if (res.equals(ResponseUsuarioService.DATOS_PERSONALES_NO_VALIDOS)) {
            return new ResponseEntity<>(ResultResponse.builder().status(false).message(HttpResponseMessage.CEDULA_NO_VALIDA.getValue()).build(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(ResultResponse.builder().status(true).message(HttpResponseMessage.PERSIST_SUCCESSFUL.getValue()).build(), HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<?> findAllUsersVO() {
        return new ResponseEntity<>(ResultResponse.builder().status(true).message(HttpResponseMessage.FIND_SUCCESSFUL.getValue()).data(usuarioService.findAllUsersVO()).build(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findAllByIdRol(@PathVariable("id") int id) {
        List<UsuarioVO> vo = usuarioService.findAllByIdRol(id);
            if(vo.isEmpty())
                return new ResponseEntity<>(ResultResponse.builder().status(false).message(HttpResponseMessage.NOT_FOUND_RECORD.getValue()).build(), HttpStatus.OK);

        return new ResponseEntity<>(ResultResponse.builder().status(true).message(HttpResponseMessage.FIND_SUCCESSFUL.getValue()).data(vo).build(), HttpStatus.OK);
    }

    @GetMapping("/vacunado/{estado}")
    public ResponseEntity<?> findAllByEstadoVacunacion(@PathVariable("estado") boolean estado) {
        List<UsuarioVO> vo = usuarioService.findAllByEstadoVacunacion(estado);
        if(vo.isEmpty())
            return new ResponseEntity<>(ResultResponse.builder().status(false).message(HttpResponseMessage.NOT_FOUND_RECORD.getValue()).build(), HttpStatus.OK);

        return new ResponseEntity<>(ResultResponse.builder().status(true).message(HttpResponseMessage.FIND_SUCCESSFUL.getValue()).data(vo).build(), HttpStatus.OK);
    }






}
