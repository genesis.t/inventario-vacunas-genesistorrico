package com.challenge.invvacunaciongt.inventariovacunaciongt.service.vacunacion;

import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.vacunacion.DetalleVacuna;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.vacunacion.dto.DetalleVacunaDTO;

public interface DetalleVacunaService {
    DetalleVacuna persistRegistroDetalleVacuna(DetalleVacunaDTO dto);
}
