package com.challenge.invvacunaciongt.inventariovacunaciongt.service.seguridad;

import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.seguridad.Usuario;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.enums.ResponseUsuarioService;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.dto.UsuarioDTO;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.vo.UsuarioVO;
import java.util.Optional;
import java.util.List;

public interface UsuarioService {
    ResponseUsuarioService persistRegistrarEmpleado(UsuarioDTO dto);
    void update(Usuario usuario);
    Optional<Usuario> findById(int id);
    List<UsuarioVO> findAllUsersVO();
    List<UsuarioVO> findAllByIdRol(int idRol);
    List<UsuarioVO> findAllByEstadoVacunacion(boolean estadoVacunacion);
    void delete(Usuario usuario);
}
