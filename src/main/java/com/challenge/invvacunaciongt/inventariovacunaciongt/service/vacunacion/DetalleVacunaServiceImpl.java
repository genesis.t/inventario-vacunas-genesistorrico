package com.challenge.invvacunaciongt.inventariovacunaciongt.service.vacunacion;

import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.vacunacion.DetalleVacuna;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.vacunacion.dto.DetalleVacunaDTO;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.repository.vacunacion.DetalleVacunaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetalleVacunaServiceImpl implements DetalleVacunaService{

    private final DetalleVacunaRepository detalleVacunaRepository;

    @Autowired
    public DetalleVacunaServiceImpl(DetalleVacunaRepository detalleVacunaRepository) {
        this.detalleVacunaRepository = detalleVacunaRepository;
    }


    @Override
    public DetalleVacuna persistRegistroDetalleVacuna(DetalleVacunaDTO dto) {
        DetalleVacuna entity = new DetalleVacuna();
        BeanUtils.copyProperties(dto, entity);
        entity.setFechaVacuna(dto.getFechaVacuna());
        entity.setNumeroDosis(dto.getNumeroDosis());
        entity.setIdTipoVacuna(dto.getIdTipoVacuna());
        entity.setIdUsuario(dto.getIdUsuario());
        return detalleVacunaRepository.save(entity);
    }


}
