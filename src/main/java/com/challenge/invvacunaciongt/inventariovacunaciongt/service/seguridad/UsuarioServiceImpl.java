package com.challenge.invvacunaciongt.inventariovacunaciongt.service.seguridad;

import com.challenge.invvacunaciongt.inventariovacunaciongt.model.entity.seguridad.Usuario;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.enums.ResponseUsuarioService;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.dto.UsuarioDTO;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.pojo.seguridad.vo.UsuarioVO;
import com.challenge.invvacunaciongt.inventariovacunaciongt.model.repository.seguridad.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService {


    private final UsuarioRepository usuarioRepository;

    @Autowired
    public UsuarioServiceImpl(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }
    @Override
    public Optional<Usuario> findById(int id) {
        return this.usuarioRepository.findById(id);
    }

    @Override
    public List<UsuarioVO> findAllUsersVO() {
        List<UsuarioVO> resultList = new ArrayList<>();
        for (Usuario entity : usuarioRepository.findAll()) {
            UsuarioVO vo = new UsuarioVO();
            BeanUtils.copyProperties(entity, vo);
            resultList.add(vo);
        }
        return resultList;
    }

    @Override
    public List<UsuarioVO> findAllByIdRol(int idRol) {
        List<Usuario> list = usuarioRepository.findAllByIdRol(idRol);
        List<UsuarioVO> result = new ArrayList<>(list.size());
        for (Usuario entity : list) {
           UsuarioVO vo = new UsuarioVO();
            BeanUtils.copyProperties(entity, vo);
            result.add(vo);
        }
        return result;
    }

    @Override
    public List<UsuarioVO> findAllByEstadoVacunacion(boolean estadoVacunacion) {
        List<Usuario> list = usuarioRepository.findAllByEstadoVacunacion(estadoVacunacion);
        List<UsuarioVO> result = new ArrayList<>(list.size());
        for (Usuario entity : list) {
            UsuarioVO vo = new UsuarioVO();
            BeanUtils.copyProperties(entity, vo);
            result.add(vo);
        }
        return result;
    }


    @Override
    public ResponseUsuarioService persistRegistrarEmpleado(UsuarioDTO dto) {
        long cedula = dto.getCedula();
        String nombres = dto.getNombres();
        String apellidos = dto.getApellidos();
        Usuario entity = new Usuario();
        BeanUtils.copyProperties(dto, entity);
        entity.setUsername(dto.getNombres());
        entity.setPassword(dto.getCedula()+"");
        if((int)(Math.log10(cedula)+1) != 10)
            return ResponseUsuarioService.CEDULA_NO_VALIDA;
        if(nombres.matches(".*[0-9].*")||apellidos.matches(".*[0-9]"))
            return ResponseUsuarioService.DATOS_PERSONALES_NO_VALIDOS;
        if(nombres.matches("[!@$#%&*()_+=|<> ?{}\\[\\]~-]")||apellidos.matches("[!@$#%&*()_+=|<> ?{}\\[\\]~-]"))
            return ResponseUsuarioService.DATOS_PERSONALES_NO_VALIDOS;

        usuarioRepository.save(entity);
        return ResponseUsuarioService.EMPLEADO_REGISTRADO;
    }

    @Override
    public void update(Usuario usuario) {
        usuarioRepository.save(usuario);
    }

    @Override
    public void delete( Usuario usuario) {
        usuarioRepository.delete(usuario);
    }




}
